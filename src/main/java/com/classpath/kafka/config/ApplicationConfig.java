package com.classpath.kafka.config;

public class ApplicationConfig {
    public final static String applicationID = "Concurrent-Producer";
    public final static String topicName = "multiple-partitions";
    public final static String bootstrapServers = "139.59.64.198:9092,157.245.98.120:9092,128.199.16.50:9092";
}
