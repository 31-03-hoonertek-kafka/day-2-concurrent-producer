package com.classpath.kafka.producer;

import com.classpath.kafka.config.ApplicationConfig;
import com.classpath.kafka.model.User;
import com.classpath.kafka.serde.JsonSerializer;
import com.github.javafaker.Faker;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.time.LocalDate;
import java.util.Properties;

public class UserProducerDemoClient {
    public static void main(String[] args) throws InterruptedException {
        Properties props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, ApplicationConfig.applicationID);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ApplicationConfig.bootstrapServers);
        props.put(ProducerConfig.ACKS_CONFIG, "0");
        String topicName = "users-topic";
        Faker faker = new Faker();
        KafkaProducer<Integer, User> kafkaProducer = new KafkaProducer<>(props);

        for(int index = 0; index < 100; index ++){
            User user = User.builder()
                                .userId(faker.number().numberBetween(24000, 50000)).name(faker.name().firstName())
                                .dob(LocalDate.now())
                                .email(faker.internet().emailAddress())
                                .build();
            ProducerRecord<Integer, User> producerRecord = new ProducerRecord(topicName, user.getUserId(), user);
            kafkaProducer.send(producerRecord);
            Thread.sleep(5000);
        }
        kafkaProducer.close();
    }
}
