package com.classpath.kafka.producer;

import com.classpath.kafka.config.ApplicationConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducerDemo {
    public static void main(String[] args) throws InterruptedException {
        Properties props = new Properties();
        props.put(ProducerConfig.CLIENT_ID_CONFIG, ApplicationConfig.applicationID);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, ApplicationConfig.bootstrapServers);
        props.put(ProducerConfig.ACKS_CONFIG, 0);
        String topicName = ApplicationConfig.topicName;
        KafkaProducer<Integer, String> kafkaProducer = new KafkaProducer<>(props);

        Thread thread1 = new Thread(new ExecutorTask(kafkaProducer, topicName));
       // Thread thread2 = new Thread(new ExecutorTask(kafkaProducer, topicName));

        thread1.start();
       // thread2.start();;

        System.out.println(" Both tasks are now started....");

        thread1.join();
        //thread//1.join();
        kafkaProducer.close();
    }
}
