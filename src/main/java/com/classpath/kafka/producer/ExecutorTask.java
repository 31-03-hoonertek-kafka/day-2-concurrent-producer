package com.classpath.kafka.producer;

import com.github.javafaker.Faker;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class ExecutorTask implements Runnable {

    private final String topicName;
    private final KafkaProducer<Integer, String> producer;
    private final Faker faker = new Faker();

    public ExecutorTask(KafkaProducer<Integer, String> producer, String topicName){
        this.producer = producer;
        this.topicName = topicName;
    }

    @Override
    public void run() {
        for(int index = 0; index < 10_000; index++){
            String fact = faker.chuckNorris().fact();
            ProducerRecord<Integer, String> producerRecord = new ProducerRecord<>(topicName, index, "Index : "+index +"~->"+ fact);
            this.producer.send(producerRecord);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
