package com.classpath.kafka.model;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
@EqualsAndHashCode
@Builder
public class User implements Serializable {
    private int userId;
    private String name;
    private String email;
    private LocalDate dob;
}
